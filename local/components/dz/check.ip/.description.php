<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("CHECK_IP_NAME"),
    "DESCRIPTION" => GetMessage("CHECK_IP_DESC"),
    "SORT" => 100,
    "PATH" => array(
        "ID" => "service",
        "SORT" => 100,
        "CHILD" => array(
            "ID" => "CHECK_IP",
            "NAME" => GetMessage("CHECK_IP_JOB"),
            "SORT" => 100
        )
    )
);