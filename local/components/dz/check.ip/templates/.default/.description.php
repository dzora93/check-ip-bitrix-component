<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateDescription = array(
    "NAME" => GetMessage("CHECK_IP_NAME"),
    "DESCRIPTION" => GetMessage("CHECK_IP_DESC"),
);