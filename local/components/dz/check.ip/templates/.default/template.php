<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
CJSCore::Init(array("jquery2"));
CJSCore::RegisterExt("langJSInit", array("lang" => $templateFolder."/script.js.php"));
CJSCore::Init(array("langJSInit"));

$this->addExternalJS($templateFolder."/js/bootstrap.min.js");
$this->addExternalJS($templateFolder."/js/jquery.mask.min.js");
$this->addExternalJS($templateFolder."/js/jquery-validation-1.19.2/jquery.validate.min.js");
$this->addExternalJS($templateFolder."/js/jquery-validation-1.19.2/additional-methods.min.js");
$this->addExternalJS($templateFolder."/js/scripts.js");

$this->addExternalCss($templateFolder."/css/bootstrap.min.css");
$this->addExternalCss($templateFolder."/css/styles.css");
?>

<script type="application/javascript">
    // для передачи параметров в class.php через ajax
    var params = <?=\Bitrix\Main\Web\Json::encode(['signedParameters'=>$this->getComponent()->getSignedParameters()])?>;
</script>

<div class="check-ip p-3">
    <div class="container">

        <?if (isset($arParams["TITLE_BLOCK"]) and $arParams["TITLE_BLOCK"] != ""):?>
            <h2 class="pb-4"><?=$arParams["TITLE_BLOCK"]?></h2>
        <?endif;?>

        <div class="row">
            <div class="col-md-4">
                <form class="check-ip__form">
                    <div class="form-group">
                        <label for="check-ip-label"><?=GetMessage("CHECK_IP")?></label>
                        <input type="text" name="ip" class="form-control check-ip__ip-address" id="check-ip-label" placeholder="<?=GetMessage("CHECK_IP_PLACEHOLDER")?>" required>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <img class="gif-loader" src="<?=$templateFolder?>/images/ajax-loader.gif"  alt="loader" />
                        <?=GetMessage("CHECK_IP_SUBMIT")?>
                    </button>
                </form>
            </div>

            <div class="col-md-8">
                <div class="check-ip__response alert"></div>
            </div>
        </div>
    </div>
</div>
