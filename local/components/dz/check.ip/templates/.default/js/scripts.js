$(document).ready(function() {

    // маска ip
    $('.check-ip__ip-address').mask('099.099.099.099');

    // валидатор ip
    $('.check-ip__form').validate({
        rules: {
            ip: {
                required: true,
                ipv4: true
            }
        },
        messages: {
            ip: {
                required: BX.message('CHECK_IP_REQUIRED'),
                ipv4: BX.message('CHECK_IP_BAD_FORMAT')
            }
        },

        // ajax запрос
        submitHandler: function() {

            $('.gif-loader').show();

            BX.ajax.runComponentAction('dz:check.ip',
            'getIp', {
                mode: 'class',
                signedParameters: params.signedParameters,
                data: {
                    post: {
                        ip: $('.check-ip__ip-address').val()
                    }
                },
            })
            .then(function(response) {
                if (response.status === 'success' && response.data !== null) {
                    $('.gif-loader').hide();
                    $('.check-ip__response').removeClass('alert-danger');
                    $('.check-ip__response').addClass('alert-success');
                    $('.check-ip__response').html('<b>' + BX.message('CHECK_IP_COUNTRY') + ':</b> ' + response.data.country + '<br> <b>' + BX.message('CHECK_IP_CITY') + ':</b> ' + response.data.city);
                }
            }).catch(function(response) {
                if (response.status === 'error' && response.errors !== null) {
                    $('.gif-loader').hide();
                    $('.check-ip__response').removeClass('alert-success');
                    $('.check-ip__response').addClass('alert-danger');
                    $('.check-ip__response').html(response.errors[0].message);
                }
            });
        }
    });

});