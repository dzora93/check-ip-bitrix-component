<?php
$MESS['CHECK_IP_REQUIRED'] = "Введите IP-адрес";
$MESS['CHECK_IP_BAD_FORMAT'] = "Введите правильный формат IP-адреса";
$MESS['CHECK_IP_COUNTRY'] = "Страна";
$MESS['CHECK_IP_CITY'] = "Населённый пункт";