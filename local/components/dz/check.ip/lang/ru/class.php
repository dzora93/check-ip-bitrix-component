<?php
$MESS['CHECK_IP_ERROR'] = 'Произошла ошибка, проверьте ваш IP-адрес или попробуйте сделать запрос позже';
$MESS['CHECK_IP_GET_SERVICE'] = 'Выберите сервис в настройках компонента';
$MESS['CHECK_IP_ERROR_SERVICE_REST'] = 'Проверьте доступность сервиса REST';
$MESS['CHECK_IP_ERROR_SERVICE_SOAP'] = 'Проверьте доступность сервиса SOAP';
$MESS['CHECK_IP_CHECK_N'] = 'Проверка №';