<?php

use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;

CModule::IncludeModule("iblock");

/**
 * Класс компонента dz:check.ip
 * Получение данных от ajax и валидация
 * Получение SOAP/REST данных
 * Работа с инфоблоками
 */

class CheckIp extends \CBitrixComponent implements Controllerable
{
    /**
     * Конфигурации Экшна
     * Можно ограничить какие методы запроса принимать (GET, POST, PUT ...)
     * Можно включить CSRF
     * Можно проверить авторизацию
     *
     * @return array
     */
    public function configureActions()
    {
        return [
            'getIp' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_POST)
                    ),
                ],
            ],
        ];
    }

    /**
     * Список нужных параметров компонента для дальнейшей работы с ними
     *
     * @return array
     */
    protected function listKeysSignedParameters()
    {
        return [
            'IBLOCK_TYPE',
            'IBLOCK_ID',
            'SERVICE'
        ];
    }

    /**
     * Получаем параметры используя подписи
     *
     * @param $signedParameters
     * @return array
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\Security\Sign\BadSignatureException
     */
    protected function getParams($signedParameters)
    {
        $signer = new \Bitrix\Main\Component\ParameterSigner;
        return $signer->unsignParameters($this->__name, $signedParameters);
    }

    /**
     * Главный экшн, вся суета движуха тут)
     *
     * @param $signedParameters
     * @param $post
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Security\Sign\BadSignatureException
     * @throws Exception
     */
    public function getIpAction($signedParameters, $post)
    {
        if (filter_var($post['ip'], FILTER_VALIDATE_IP)) {

            $arParams = $this->getParams($signedParameters);

            if ($arParams['SERVICE'] === 'REST') {
                $response = $this->getRestInfo($post['ip']);
            } elseif ($arParams['SERVICE'] === 'SOAP') {
                $response = $this->getSoapInfo($post['ip']);
            } else {
                throw new Exception(GetMessage('CHECK_IP_GET_SERVICE'));
            }

            if ($response) {
                $arResponse = \Bitrix\Main\Web\Json::decode($response);
            } else {
                throw new Exception(GetMessage('CHECK_IP_ERROR_SERVICE_'.$arParams['SERVICE']));
            }

            if ($arResponse !== null or $arResponse['status'] === 'success') {

                $this->setDataIBLock($post['ip'], $arParams, $response);

                $ajaxResponse = [
                    'REST' => [
                        'country' => $arResponse['country'],
                        'city' => $arResponse['city'].', '.$arResponse['regionName']
                    ],
                    'SOAP' => [
                        'country' => $arResponse['Country'],
                        'city' => $arResponse['State']
                    ]
                ];

                return $ajaxResponse[$arParams['SERVICE']];

            } else {
                throw new Exception(GetMessage('CHECK_IP_ERROR'));
            }

        } else {
            throw new Exception(GetMessage('CHECK_IP_ERROR'));
        }
    }

    /**
     * REST http://IP-api.com/docs/api:json
     *
     * @param $ip
     * @return bool|false|mixed|string
     */
    protected function getRestInfo($ip)
    {
        return file_get_contents('http://ip-api.com/json/'.$ip.'?lang='.LANGUAGE_ID);
    }

    /**
     * SOAP http://wsgeoIP.lavasoft.com/IPservice.asmx
     * Можно было бы использовать CSOAPClient Битрикса, но редакция не позволяет
     *
     * @param $ip
     * @return bool|false|mixed|string
     */
    protected function getSoapInfo($ip)
    {
        $xmlUrl = file_get_contents('http://wsgeoip.lavasoft.com//IPservice.asmx/GetIpLocation_2_0?sIp='.$ip);
        $xml = simplexml_load_string($xmlUrl);
        $xml = simplexml_load_string((string)$xml[0]);
        return json_encode($xml);
    }

    /**
     * Работа с инфоблоком
     *
     * Получаем сначало ID последнего элемента для нумерации в названии
     * Затем создаем файл json.gz, подготавливаем данные в нужный формат
     * Пишем в ИБ
     *
     * @param $ip
     * @param $arParams
     * @param $responseJson
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     */
    public function setDataIBLock($ip, $arParams, $responseJson)
    {
        $lastId = 0;
        $properties = array();
        $jsonGz = $this->makeJsonGz($responseJson);

        $arResponse = \Bitrix\Main\Web\Json::decode($responseJson);
        $arResponse = array_change_key_case($arResponse, CASE_LOWER);

        $arSelect = Array("ID", "IBLOCK_ID");
        $arFilter = Array("IBLOCK_ID" => $arParams['IBLOCK_ID']);
        $res = CIBlockElement::GetList(Array('id' => 'desc'), $arFilter, false, Array("nPageSize" => 1), $arSelect);

        if ($arRes = $res->GetNext()) {
            $lastId = $arRes['ID'] + 1;
        }

        $element = new CIBlockElement;

        $properties['IP_ADDRESS_CLIENT'] = \Bitrix\Main\Service\GeoIp\Manager::getRealIp();
        $properties['IP_ADDRESS_FORM'] = $ip;
        $properties['RESPONSE_COUNTRY'] = $arResponse['country'];
        $properties['RESPONSE_JSON'] = CFile::MakeFileArray($jsonGz);

        $arLoadElementArray = Array(
            "MODIFIED_BY"       => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"         => $arParams['IBLOCK_ID'],
            "PROPERTY_VALUES"   => $properties,
            "NAME"              => GetMessage('CHECK_IP_CHECK_N').' '.$lastId,
            "ACTIVE"            => "Y",
        );

        $elementId = $element->Add($arLoadElementArray);
        unlink($jsonGz);

        if ($elementId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Формируем сжатые gz json-данные и отдаем url до файла
     *
     * @param $json
     * @return string
     */
    protected function makeJsonGz($json)
    {
        $gz = gzopen(__DIR__.'/json.gz','w9');
        gzwrite($gz, $json);
        gzclose($gz);
        return __DIR__.'/json.gz';
    }

    /**
     * Execute Component
     *
     * @return mixed|void|null
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }
}