<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule("iblock");

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));

$arIBlocks = array();
$db_iblock = CIBlock::GetList(
    array("SORT" => "ASC"),
    array(
        "SITE_ID" => $_REQUEST["site"],
        "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-" ? $arCurrentValues["IBLOCK_TYPE"] : "")
    )
);

while ($arRes = $db_iblock->Fetch()) {
    $arIBlocks[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];
}

$arComponentParameters = array(
    "PARAMETERS" => array(
        "TITLE_BLOCK" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CHECK_IP_TITLE_BLOCK"),
            "TYPE" => "STRING",
            "DEFAULT" => ""
        ),
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CHECK_IP_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CHECK_IP_IBLOCK_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '={$_REQUEST["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "SERVICE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("CHECK_IP_SERVICE"),
            "TYPE" => "LIST",
            "VALUES" => array(
                'REST' => 'REST (IP-api.com)',
                'SOAP' => 'SOAP (wsgeoIP.lavasoft.com)',
            ),
        ),
    ),
);